# Description
A simple role which puts an HTML file in the apache document root folder, in order to redirect to another url.  

# Usage

This role supports the following variables:  
* www_html_redirect_page_title: give a title to the redirect page  
* www_html_redirect_page_url: url to which the user has to be redirected  

# Dependencies:
This role depends on the dledanseur.apache2 role  



